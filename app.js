const express = require('express')
const bodyParser = require('body-parser') ; 
const app = express()

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const braintree = require('braintree');
const customerInfo = {};
const fs = require('fs');


let gateway = {};

let appFn = {};




appFn.createCreditCard = (res, customerInfo, creditCardParams) => {
    const customerId = customerInfo.customerId

    gateway.creditCard.create(creditCardParams, function (err, response) {
        customerInfo.creditCard = response.creditCard;
        // fs.writeFileSync('./customerData.json', JSON.stringify(customerInfo))
        // res.send('created-user!')
        
        console.log('createCreditCard response', response)
        console.log('createCreditCard token customerInfo.creditCard.token',customerInfo.creditCard.token)
        appFn.subscribeCardPaymentToRecurringPlan(res, customerInfo.creditCard.token)
    });

}


appFn.subscribeCardPaymentToRecurringPlan = (res, creditCardToken) => {
    gateway.subscription.create({
        paymentMethodToken: creditCardToken,
        planId: 'CampFyreMagic001'
    }, function (err, result) {
        // customerData.plan = result
        res.send('Subscribed   to recurring!')
    });

}

appFn.connect = (res) => {
    gateway = braintree.connect({
        environment: braintree.Environment.Sandbox,
        merchantId: 'sdw97ct4q6654h28',
        publicKey: 'h83bnx9sps2wp7x3',
        privateKey: '3ba6a675f91774622f6eeffe8e12f064'
    });

}


app.get('/connect', function (req, res) {
    appFn.connect(res)
    res.send('Connected');

})


app.get('/subscribe-user', function (req, res) {
    const customerData = JSON.parse(fs.readFileSync('customerData.json', 'utf-8'));
    appFn.subscribeCardPaymentToRecurringPlan(res, customerData.creditCard.creditCard.token)

})


app.get('/create-user', function (req, res) {

    let userInfo = {
        firstName: "Jone",
        lastName: "Smith",
        company: "Braintree",
        email: "jen@example.com",
        phone: "312.555.1234",
        fax: "614.555.5678",
        website: "www.example.com",



    }
    //   creditCard: {
    //         billingAddress: {
    //             firstName: "Jen",
    //             lastName: "Smith",
    //             company: "Braintree",
    //             streetAddress: "123 Address",
    //             locality: "City",
    //             region: "State",
    //             postalCode: "12345"
    //         } 
    //     },





    gateway.customer.create(userInfo, function (err, result) {
        userInfo.success = result.success;
        userInfo.customerId = result.customer.id;

        let creditCardParams = {
            customerId: userInfo.customerId,
            number: '4111111111111111',
            expirationDate: '06/2022',
            cvv: '100'
        };

        // // e.g. 494019
        // console.log('userInfo', userInfo);
        // fs.writeFileSync('./userInfo.json', JSON.stringify(userInfo))
        appFn.createCreditCard(res, userInfo, creditCardParams);

    });

})
/**
 * Body 
 * 
 {    
       "creditCard":{ 
            "number": "4111111111111111",
            "expirationDate": "06/2022",
            "cvv": "100"
        },
        "userInfo": {
                "firstName": "Jone",
                "lastName": "Smith",
                "company": "Braintree",
                "email": "jen@example.com",
                "phone": "312.555.1234",
                "fax": "614.555.5678",
                "website": "www.example.com" 
        }


    }
 */
app.post('/subscribe-user-recurring', function (req, res) { 
    const userInfo = req.body.userInfo || {}
    let creditCardParams = req.body.creditCard || {}

    appFn.connect(res)

    gateway.customer.create(userInfo, function (err, result) {
        userInfo.success = result.success;
        userInfo.customerId = result.customer.id;

        // create credit card
        creditCardParams['customerId'] = userInfo.customerId
        appFn.createCreditCard(res, userInfo, creditCardParams);

    });

})
app.get('/', function (req, res) {

    const gateway = braintree.connect({
        environment: braintree.Environment.Sandbox,
        merchantId: 'sdw97ct4q6654h28',
        publicKey: 'h83bnx9sps2wp7x3',
        privateKey: '3ba6a675f91774622f6eeffe8e12f064'
    });
    res.send('Hello World!')

})



const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }))  ;
app.use(bodyParser.json());

app.listen(port, function () {
    console.log('app listening on port 3000!')
})

